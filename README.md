# Desenvolvimento Web Completo

## Sumario
- ### [Descrição](#descrição)
- ### Conteúdo do Curso
    - #### [Módulo 1](#módulo-1)
    - #### [Módulo 2](#módulo-2)
    - #### [Módulo 3](#módulo-3)

# Descrição

<p>Esse repositório é para postar os códigos referente ao curso de <a href= "https://www.udemy.com/course/web-completo/">Desenvolvimento web Completo - Udemy</a>. Cada módulo do curso realizado será postado em uma branch diferente</p>

# Módulo 1

<p align="center"> Nesse módulo explica como será o funcionamento do curso e como funciona a plataforma da Udemy </p>

# Módulo 2

<p align="center"> Esse módulo ele ensina como instalar o editor de texto  <a href="https://www.sublimetext.com/">Sublime Text</a> </p>

# Módulo 3

<p align="center">Esse modulo tem como objetivo ensinar a usar o HTML5</p>